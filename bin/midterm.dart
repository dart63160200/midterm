
import 'dart:io';


void main() {

  print("input expression : ( 1+2)*7");
  //String expression = stdin.readLineSync()!; // รับ expression
  String expression = "( 1+2)*7";

  print("Infix : ");
  print(tokenizing(expression)); // ปริ้น infix

  String infix = tokenizing(expression).toString(); // เเปลง List เป็น String
  print(" Postfix : ");
  print(postfix(infix)); //ปริ้น postfix

  String evaluate = postfix(infix).toString(); // เเปลง List เป็น String
  print("Evaluate Postfix : ");
  print(Evaluate_postfix(evaluate)); // ปริ้น evaluate postfix
}

List tokenizing(String expression) {// ตัดคำ
  List list_infix = []; //  สร้าง list_infix ว่าง ไว้เก็บ infix
  for (int i = 0; i < expression.length; i++) {  //วนลูปในเช็คที่ละตัว
  
    list_infix.add(expression.substring(i, i + 1)); // เพิ่มคำที่ละตัวใน list_infix
    if (list_infix.contains(" ") == true) { // ถ้าเจอช่องว่าง
      
      list_infix.remove(" "); // ให้ลบช่องว่างออกจาก list_infix
    }
  }
  return list_infix; // ส่งค่า list_infix กลับขึ้นไปที่ main
}

List postfix(String infix) { // function postfix
 
  List list_posfix = []; //  สร้าง list_posfix ว่าง ไว้เก็บตัวเลข
  List list_operators =[]; //สร้าง list_operators ว่างไว้เก็บ operators + - * / ( )

  for (int i = 0; i < infix.length; i++) {
    if (infix_i_Is_Integer(infix, i)) {  // ถ้า infix ตัวที่ i เป็นตัวเลข 0-9

      list_posfix.add(infix[i]); // เพิ่มลงใน list_posfix
    }

    if (infix_i_Is_Operators(infix, i)) {// ถ้า infix ตัวที่ i เป็น operators + - * /
      

      while (list_operators.isNotEmpty && list_operators.last != "(" && precedence(infix[i]) < precedence(list_operators.last)) {
        // ถ้า list_operators ไม่ว่าง เเละ ตัวสุดท้ายใน list_operators ไม่เป็น ( วงเล็บเปิด เเละ infix ตัวที่ i มีค่าน้อยกว่า ตัวสุดท้ายใน list_operators

        list_posfix.add(list_operators.last); // เพิ่ม  list_operators ตัวสุดท้ายเข้า list_posfix
        list_operators.removeLast(); // ลบตัวสุดท้ายใน list_operators ออก

      }
      list_operators.add(infix[i]);
    }

    if (infix[i] == "(") {// ถ้า infix ตัวที่ i เป็น ) วงเล็บเปิด
      
      list_operators.add(infix[i]); //เพิ่ม operator ลงใน list_operators
    }
    if (infix[i] == ")") {// ถ้า infix ตัวที่ i เป็น ) วงเล็บเปิด
      
      while (list_operators.last != "(") { //ถ้าตัวสุดท้ายของ list_operators.last ไม่ใช่ วงเล็บเปิด
       
        list_posfix.add(list_operators.last); // เพิ่มตัวสุดท้ายของ list_operators.last ใน list_posfix
        list_operators.removeLast(); // ลบตัวสุดท้ายของlist_operators
      }
      list_operators.remove("("); //ลบ วงเล็บเปิด ออกจาก list_operators

    }
  }
  while (list_operators.isNotEmpty) { // ถ้า list_operators ไม่ว่าง
   
    list_posfix.add(list_operators.last); // เพิ่ม ตัวสุดท้ายที่อยู่ใน list_operators ใน list_posfix
    list_operators.remove(list_operators.last); // ลบ ตัวสุดท้ายที่อยู่ใน list_operators
  }
 
  return list_posfix; //ส่งค่า list_posfix กลับ main
}

bool infix_i_Is_Operators(String infix, int i) { // infix ตัวที่ i เป็นตัวเลข operator
  return infix[i] == "+" ||
      infix[i] == "-" ||
      infix[i] == "*" ||
      infix[i] == "/";
}

bool infix_i_Is_Integer(String infix, int i) { // infix ตัวที่ i เป็นตัวเลข 0-9
  return infix[i] == "0" ||
      infix[i] == "1" ||
      infix[i] == "2" ||
      infix[i] == "3" ||
      infix[i] == "4" ||
      infix[i] == "5" ||
      infix[i] == "6" ||
      infix[i] == "7" ||
      infix[i] == "8" ||
      infix[i] == "9";
}

int precedence(String oper) { // function ให้ค่าของ operators
 
  switch (oper) {
    case "+":
    case "-":
      return 1; // + - ให้ค่า 1
    case "*":
    case "/":
      return 2; // * / ให้ค่า 2
    default:
      return -1;
  }
}

double Evaluate_postfix(String postfix) {// function  Evaluate_postfix
  
  List list_evaluate = []; // สร้าง List เก็บค่าตัวเลข
  //List list_posfix=[];
  double evaluate = 0;

  for (int i = 0; i < postfix.length; i++) {

      if (post_i_Is_Integer(postfix, i)) { // ถ้า postfix ตัวที่ i เป็นตัวเลข
        
        double number =  double.parse(postfix[i]); //int.parse(postfix[i]); // เเปลง postfix ตัวที่ i จาก String เป็น integer
        list_evaluate.add(number); // เพิ่ม number ใส่ใน list_evaluate
        //print(list_evaluate);

      }
      if(postfix[i]== "+"){ // ถ้า postfix[i] เป็น + บวก
          double num1 = list_evaluate.removeLast(); 
          double num2 = list_evaluate.removeLast();
          evaluate = num1 + num2 ; // บวก
          list_evaluate.add(evaluate); //เพิ่ม ค่า evaluate ใน list_evaluate

      }
      if(postfix[i]== "-"){ // ถ้า postfix[i] เป็น - ลบ
          double num1 = list_evaluate.removeLast(); 
          double num2 = list_evaluate.removeLast();
          evaluate = num1 - num2 ;
          list_evaluate.add(evaluate);//เพิ่ม ค่า evaluate ใน list_evaluate
          
      

      }
      if(postfix[i]== "*"){ // ถ้า postfix[i] เป็น * คูณ
          double num1 = list_evaluate.removeLast(); 
          double num2 = list_evaluate.removeLast();
          evaluate = num1 * num2 ;
          list_evaluate.add(evaluate);//เพิ่ม ค่า evaluate ใน list_evaluate
      
      }
      if(postfix[i]== "/"){ // ถ้า postfix[i] เป็น / หาร
          double num1 = list_evaluate.removeLast();
          double num2 = list_evaluate.removeLast();
          evaluate = num1/num2;
          list_evaluate.add(evaluate);//เพิ่ม ค่า evaluate ใน list_evaluate
      }
  }
  return evaluate; //ส่งค่ากลับไปที่ main
}

bool post_i_Is_Integer(String postfix, int i) { // postfix ตัวที่ i เป็นตัวเลข 0-9
  return postfix[i] == "1" ||
        postfix[i] == "2" ||
        postfix[i] == "3" ||
        postfix[i] == "4" ||
        postfix[i] == "5" ||
        postfix[i] == "6" ||
        postfix[i] == "7" ||
        postfix[i] == "8" ||
        postfix[i] == "9";
}
